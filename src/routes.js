import { Router } from 'express';
import ProductController from './app/controllers/ProductController';

const routes = new Router();

routes.post('/product', ProductController.store);
routes.get('/products', ProductController.index);
routes.post('/filterProduct', ProductController.filter);

export default routes;
