import Product from '../schemas/Product';

class ProductController {
  async store(req, res) {
    const response = await Product.create(req.body);
    return res.status(200).json(response);
  }

  async index(req, res) {
    const response = await Product.find();
    return res.status(200).json(response);
  }

  async filter(req, res) {
    const { storeAddress, productName, productType, quantity } = req.body;

    const response = await Product.find({
      address: { $regex: storeAddress, $options: 'i' },
      'products.name': productName,
      'products.type': productType,
      'products.quantity': { $gte: quantity },
    });

    return res.status(200).json(response);
  }
}

export default new ProductController();
