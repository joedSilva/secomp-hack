const axios = require('axios');

async function main(params) {

  params = {
    'storeAddress': params.storeAddress,
    'productName': params.productName,
    'productType': params.productType,
    'quantity': params.quantity
  }

  const response = await axios.post('https://secomp-hack.herokuapp.com/filterProduct', params);

  const store = response.data[0];

  let storeName = '';
  let storeTelephone = '';

  if (store) {
    storeName = store.name;
    storeTelephone = store.telephone_number;

    return { result: { storeName, storeTelephone } };
  }

  else {
    return { result: { err: true } };
  }
}
