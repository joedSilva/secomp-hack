<p align="center">
<img src="https://catalisi.com.br/wp-content/uploads/2019/07/catalisi-ab-inbev-logo.jpg" height="150" width="300">
</p>
<h1>Cheers - Hackahton SECOMP</h1>

### Introdução
Você já passou pela situação onde queria comprar determinado produto na sua região mas não sabia exatamente onde encontrar? No domingo à tarde saiu para comprar aquela cerveja e quando chegou no estabelecimento, viu que não tinha a que você queria?
Pois bem, e se eu te dissesse que é possível você descobrir onde encontrar seus produtos através do seu aplicativo de mensagens favorito como o Whatsapp ou o Messenger?
O Cheers veio para te ajudar.


### A ideia
Vivemos em um mundo onde as pessoas vivem conectadas e gostam muito de usar o Whatsapp no seu dia a dia. Com a pandemia, o aplicativo se mostrou muito eficiente,
no que se refere a ajudar comerciantes a se comunicar com seus clientes. Aproveitamos essa facilidade para criar um novo serviço capaz de ajudar consumidores a encontrar estabelecimentos.

Basicamente, com o Cheers você se comunica com um chatbot onde ele te faz algumas perguntas como o local que você está e o tipo de produto que você quer. Com essas
informações ele busca no sistema os estabelecimentos cadastrados que possuem o produto que você deseja e assim te indica aquele mais apropriado. Com isso,
evitamos que tanto o cliente como o vendedor se sintam com a sensação de tempo perdido.


### Ferramentas/Tecnologias
- IBM Watson Assistant
- IBM Cloud Functions
- Twilio
- Heroku
- MongoDB
- NodeJS

### Execução
Utilizamos o IBM Watson Assistant para conseguir construir um chatbot capaz de entender linguagem natural e assim tornar o atendimento mais humanizado. Para integrar isso
com o Whatsapp, utilizamos o serviço da Twilio, no entanto o chatbot pode ser integrado em qualquer sistema de mensagens como o messenger por exemplo. 


### Como testar
Enviar a mensagem "join full-steady" para +1 415 523 8886.

O bot ainda está em fase inicial, por isso ainda pode ocorrer falhas durantes o uso.


### Roadmap
- Adicionar mais opçoes de produtos.
- Conseguir vender direto pelo chatbot.
- Gerar estatísticas dos pedidos.

### Links
Pitch: https://www.youtube.com/watch?v=g3mU5y8oU7Y&rel=0
Demo: https://www.youtube.com/watch?v=yZZ0Po7g4DM

Enjoy =)


